<?php
	class ListsController{
		public function all(){
			if($_SERVER['REQUEST_METHOD'] === 'POST'){
				$Adder = new Adder();
				$Adder->emptyLists();
				$Adder->addToday();
			}

			$ListLoader = new ListLoader();
			$lists = $ListLoader->loadAllLists();
			$total = $ListLoader->loadTotalList();
			require_once('views/lists/lists.php');
		}

		public function one(){

			if(!isset($_GET["name"])){ //check in certain name of list is in GET
				return call('pages', 'error'); //if not return error page
			}
			$ListLoader = new ListLoader();
			$list = $ListLoader->loadListFromDatabase($_GET["name"]); //load list with certain name
			require_once('views/lists/single_list.php');

		}

	}