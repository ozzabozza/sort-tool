<?php
	class CheckerController{

		public function home(){
			if($_SERVER['REQUEST_METHOD'] === 'POST' && !empty($_POST)){
				session_start();
				$_SESSION["$postcode"] = $_POST["postcode"];

				$Checker = new Checker();
				$matches = $Checker->getMatches($_POST["postcode"]);
				$matches2 = $Checker->getMatchesTotal($_POST["postcode"]);

				if(count($matches) == 0 && count($matches2) == 0){
					require_once('views/checker/home.php');
					require_once('views/checker/matches/no_match.php');
				}

				else{
					require_once('views/checker/home.php');
					require_once('views/checker/matches/matches_form.php');
				}

			}
			require_once('views/checker/home.php');
		}

		public function check(){
			if($_SERVER['REQUEST_METHOD'] === 'POST' && !empty($_POST)){
				session_start();
				$Checker = new Checker();
				$Checker->checkMatches($_POST["match"], $_POST["match2"]);

				$Checker->errorHandler($_POST, $_SESSION["$postcode"]);
				
			}

			require_once('views/checker/home.php');
		}

		public function errors(){
			$Checker = new Checker();
			$errors = $Checker->getErrors();
			require_once('views/checker/errors.php');
		}

	}