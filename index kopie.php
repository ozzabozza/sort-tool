<?php
	session_start();
	require("includes/mysql_connect.php");
	require("includes/functions.php");

	render("index.php", ["title" => "Checken"]);

if ($_SERVER["REQUEST_METHOD"] == "POST"){

	if($_POST["keuze_zending"] !== NULL || $_POST["keuze_totaal"] !==NULL){
		
		if($_POST["keuze_zending"] !== NULL){
			check_zending($_POST["keuze_zending"]);
		}

		elseif($_POST["keuze_zending"] == NULL){
			if(!$_POST["auto"]){
				noteer_nietoplijst($_SESSION["postcode"], "Wel op FKS sorteerlijst niet op Cycloon routelijst");
			}
		};

		if($_POST["keuze_totaal"] !== NULL){
			check_totaal($_POST["keuze_totaal"]);
		}

		elseif($_POST["keuze_totaal"] == NULL){
			if(!$_POST["nietfks"] == TRUE){
				noteer_nietoplijst($_SESSION["postcode"], "Wel op Cycloon routelijst niet op FKS sorteerlijst");
			}
		}
	}

	elseif ($_POST["beideniet"] !== NULL) {
		noteer_nietoplijst($_SESSION["postcode"], $_SESSION["nummer"], "Op beide lijsten niet te vinden!");
	}

	else{

		$_SESSION["postcode"] = strtoupper($_POST["postcode"]);
		$postcode = strtoupper($_POST["postcode"]);
	
		//$x = 0;

		$matches = check_matches_zending($postcode);
		$matches2 = check_matches_totaal($postcode);
		
		toon_resultaten($matches, $matches2);
	}
};
?>
<script type="text/javascript">
	$('.table').on('click', '.clickable-row', function(event) {
  		$(this).addClass('active').siblings().removeClass('active');
  		$(this).parent().find(':checkbox').prop('checked', false);
  		$(':checkbox', this).prop('checked', true);
	})
</script>