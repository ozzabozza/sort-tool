<?php
 	class Checker{

 		private $matches = [];
 		private $matches2 = [];

 		private $pdo;

 		public function __construct(){
 			$this->pdo = Db::getInstance();
 		}

		public function getMatches($postcode){

    	    $statement = $this->pdo->prepare('SELECT * FROM addresses WHERE checked = 0');
    	    $statement->execute();

			$rows = $statement->fetchall(PDO::FETCH_ASSOC);

    	    foreach ($rows as $row){
    	        if (strpos($row['address'], $postcode) !== false) { //if postcode is in it
    	                $this->matches[] = $row;
    	        }
    	    }
    	    return $this->matches;
    	}

    	public function getMatchesTotal($postcode){
    	    $statement = $this->pdo->prepare('SELECT * FROM total WHERE checked = 0');
    	    $statement->execute();

    	    $rows = $statement->fetchall(PDO::FETCH_ASSOC);
	
    	    foreach ($rows as $row) {
    	        if (strpos($row['postcode'], $postcode) !== false) { //if postcode is in it
    	            $this->matches2[] = $row;
    	        }
    	    }
    	    return $this->matches2;
    	}

    	public function checkMatches($match, $match2){
    		$statement = $this->pdo->prepare('UPDATE addresses SET checked = 1 WHERE id = :id');
    	    $statement->execute(array('id' => $match));

    	    $statement = $this->pdo->prepare('UPDATE total SET checked = 1 WHERE id = :id');
    	    $statement->execute(array('id' => $match2));
    	}

    	public function errorHandler($post, $postcode){

			$statement = $this->pdo->prepare('INSERT INTO errors(`postcode`, `comment`) VALUES (:postcode, :comment)');

    		if(isset($post["match"]) && isset($post["match2"])){
    		}

            elseif(!isset($post["match"]) && !isset($post["match2"])){
                $statement->execute(array('postcode' => $postcode, 'comment' => 'Op beide lijsten niet'));
            }

            elseif(isset($post["match"]) && !isset($post["no_match2"])){
                $statement->execute(array('postcode' => $postcode, 'comment' => 'Niet op Sorteerlijst'));
            }
    		elseif(isset($post["match2"]) && !isset($post["no_match"])){
    			$statement->execute(array('postcode' => $postcode, 'comment' => 'Niet op Cycloonlijst'));
    		}
    		elseif(isset($post["no_matches"]))
    			$statement->execute(array('postcode' => $postcode, 'comment' => 'Op beide lijsten niet'));
    	}

        public function getErrors(){
        	$statement = $this->pdo->prepare('SELECT * FROM errors');
            $statement->execute();
            $errors = $statement->fetchall(PDO::FETCH_ASSOC);
            return $errors;
        }
	}
 ?>