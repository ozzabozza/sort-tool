<?php
	class Adder{

		private $pdo;

		public function __construct(){
			$this->pdo = Db::getInstance();
		}

		public function addToday(){

			$statement = $this->pdo->prepare("INSERT INTO addresses(`number`, `address`, `courier`) VALUES (:x, :string, :name)");

			if ($_SERVER["REQUEST_METHOD"] == "POST"){
				foreach ($_POST as $list => $key) {
						if (!empty($key) && $list !== 'total'){

							$addresses = explode("\n", $key);
							$x = 0;
							foreach ($addresses as $address) {
								$x++;
								$statement->execute(array('x' => $x, 'string' => $address, 'name' => $list));
							}
						}
						if($list == 'total'){
							$statement = $this->pdo->prepare("INSERT INTO total(`postcode`, `route`) VALUES (:postcode, :route)");
							$addresses = explode("\n", $key);
							foreach ($addresses as $address) {
								$parts = array();
								$parts = preg_split('/\s+/', $address);
								$statement->execute(array('postcode' => $parts[0], 'route' => $parts[1]));
							}
						}
				}
			}
		}

		public function emptyLists(){
			$statement = $this->pdo->prepare("TRUNCATE TABLE addresses");
			$statement->execute();
			$statement = $this->pdo->prepare("TRUNCATE TABLE total");
			$statement->execute();
			$statement = $this->pdo->prepare("TRUNCATE TABLE errors");
			$statement->execute();
		}
	}
?>