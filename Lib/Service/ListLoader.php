<?php
	require_once('lib/models/AddressList.php');

	class ListLoader{

		private $pdo;

		public $listoflists = []; 

		public function __construct(){

			$databaseName = 'sorting_tool';
			$databaseUser = 'root';
			$databasePassword = 'root';
		}

		public function loadAllLists(){

			$pdo = Db::getInstance();

			$statement = $pdo->prepare("SELECT DISTINCT courier FROM addresses");
			$statement->execute();
			$couriers = $statement->fetchAll(PDO::FETCH_COLUMN);

			$statement = $pdo->prepare('SELECT * FROM addresses');
			$statement->execute();
			$allAdresses = $statement->fetchall(PDO::FETCH_ASSOC);

			foreach($couriers as $courier){
				$list = new AddressList($courier);
				
				foreach($allAdresses as $address){
					if($address["courier"] == $courier){
						$list->addresses[] = $address;
					}
				}

				$this->listoflists[] = $list;
			}

			return $this->listoflists;
		}

		public function loadListFromDatabase($name){

			$pdo = Db::getInstance();

			$statement = $pdo->prepare('SELECT * FROM addresses WHERE courier = :name');
			$statement->execute(array(":name" => $name));

			$list = new AddressList($name);
			$list->addresses = $statement->fetchall(PDO::FETCH_ASSOC);

			return $list;
		}

		public function loadTotalList(){

			$pdo = Db::getInstance();

			$statement = $pdo->prepare('SELECT * FROM total');
			$statement->execute();
			$total = new AddressList('Sorteerlijst');
			$total->addresses = $statement->fetchall(PDO::FETCH_ASSOC);

			return $total;
		}
		
	}