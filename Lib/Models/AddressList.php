<?php 

	class AddressList{
	
		private $name;

		public $addresses = [];

		public function __construct($courier){

			//set list name
			$this->name = $courier;

		}

		public function getName(){
			return $this->name;
		}

		public function getAddresses(){
			return $this->addresses;
		}
		
	}
?>