<?php
	function call($controller, $action){
		require_once('controllers/' . $controller . '_controller.php');

		switch ($controller) {
			case 'pages':
				$controller = new PagesController();
				break;
			case 'lists':
				require_once('lib/service/ListLoader.php'); //model or service
				require_once('lib/service/Adder.php'); //dont know if this is good practice
				$controller = new ListsController();
				break;
			case 'add':
				require_once('lib/service/Adder.php'); //model or service
				$controller = new AddController();
				break;
			case 'checker':
				require_once('lib/service/Checker.php');
				$controller = new CheckerController();
				break;
		}

		$controller->{ $action }();
	}

		//Array of all controllers with its actions
	$controllers = array(	'pages' => ['home', 'error'],
							'lists' => ['all', 'one'],
							'add' => ['addtoday', 'add'],
							'checker' => ['home', 'check', 'errors']);

	if(array_key_exists($controller, $controllers)){
		if(in_array($action, $controllers[$controller])){
			call($controller, $action);
		}
		else{
			call('pages', 'error');
		}
	}
	else{
		call('pages', 'error');
	}
	