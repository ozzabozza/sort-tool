<?php
/*
* DATABASE SETTINGS
*/

$databaseName = 'sorting_tool';
$databaseUser = 'root';
$databasePassword = 'root';

/*
* CREATE DATABASE
*/

$pdoDatabase = new PDO('mysql:host=localhost', $databaseUser, $databasePassword);
$pdoDatabase->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$pdoDatabase->exec("CREATE DATABASE IF NOT EXISTS `$databaseName`");

/*
* CREATE TABLES
*/

$pdo = new PDO('mysql:host=localhost;dbname='.$databaseName, $databaseUser, $databasePassword);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$pdo->exec('DROP TABLE IF EXISTS addresses');

$pdo->exec('CREATE TABLE `addresses` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	`courier` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	`number` int(4) NOT NULL,
	`checked` int(4) NOT NULL,
	`date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');

$pdo->exec('DROP TABLE IF EXISTS total');

$pdo->exec('CREATE TABLE `total` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`postcode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	`route` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	`checked` int(4) NOT NULL,
	`date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');

$pdo->exec('DROP TABLE IF EXISTS errors');

$pdo->exec('CREATE TABLE `errors` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`postcode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	`comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	`date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci');

echo "Done, database tables created!\n";
