<div class="container">
    <form name="form" method="post" action="?controller=checker&action=check">
        <fieldset>
        	<?php
        		require_once('views/checker/matches/matches.php');
				require_once('views/checker/matches/matches_total.php');
        	?>
 				<div class="form-group">
                    <button type="submit" class="btn btn-default" id="keuze" name="submit">Bevestig</button>
                    <button class="btn btn-default"  href="/">Annuleer</button>
                </div>
        </fieldset>         
    </form>
</div>

<script type="text/javascript">
    $('.table').on('click', '.clickable-row', function(event) {
        $(this).addClass('active').siblings().removeClass('active');
        $(this).parent().find(':checkbox').prop('checked', false);
        $(':checkbox', this).prop('checked', true);
    })
</script>