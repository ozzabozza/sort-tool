<div class="container">
    <form name="form" method="post" action="?controller=checker&action=check">
        <fieldset>
                <div class="alert alert-danger">
                    <h3>Op beide lijsten niet</h3>
                    <label><div class="checkbox col-xs-12">
                    <input type="checkbox" name="no_matches" value="TRUE" checked><?php echo $_POST["postcode"];?> Op beide lijsten niet gevonden</label></div>
                </div>
 				<div class="form-group">
                    <button type="submit" class="btn btn-default" id="keuze" name="submit">Bevestig</button>
                    <button class="btn btn-default"  href="/">Annuleer</button>
                </div>
        </fieldset>         
    </form>
</div>

<script type="text/javascript">
    $('.table').on('click', '.clickable-row', function(event) {
        $(this).addClass('active').siblings().removeClass('active');
        $(this).parent().find(':checkbox').prop('checked', false);
        $(':checkbox', this).prop('checked', true);
    })
</script>