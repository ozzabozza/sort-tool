<?php 
  if(count($matches) == 0){
            echo '  
                    <div class="alert alert-danger">
                        <h3>Routelijst Cycloon</h3>
                        <b>Niet op routelijst Cycloon</b>
                        <div class="checkbox">
                        <label>
                            <input type="checkbox" name="no_match" value="TRUE" checked>Dit is een auto pakket
                        </label>
                        </div>
                    </div>';
  }
  else{
    echo '
    <div class="alert alert-success">
         <h3>Routelijst Cycloon</h3>
             <table class="table">
                 <thead>
                     <tr>
                         <th>
                             Koerier
                         </th>
                         <th>
                             Nr
                         </th>
                         <th>
                             Adres
                         </th>
                         <th>
                             Kies
                         </th>
                     </tr>
                 </thead>
             <tbody>';
        
    foreach($matches as $match){
      echo '
                     <tr class="clickable-row">
                         <td>
                             '.$match["courier"].'
                         </td>
                         <td>
                              '.$match["number"].'
                         </td>
                          <td>
                              '.$match["address"].'
                         </td>
                         <td>    
                             <div class="checkbox col-xs-3 col-sm-offset-2s">
                                 <input type="checkbox" name="match" value="'.$match["id"].'">
                             </div>
                         </td>         
                     </tr>';
    }

  echo '
                        </tbody>
                    </table>
                </div>';

        }

    