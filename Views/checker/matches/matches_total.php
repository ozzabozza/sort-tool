<?php 
  if(count($matches2) == 0){
    echo '  
            <div class="alert alert-danger">
                <h3>Sorteerlijst FKS</h3>
                <b>Niet op sorteerlijst FKS</b>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="no_match2" value="TRUE" checked>Dit is geen fks pakket
                    </label>
                </div>
            </div>';
  }
        
  else{
    echo '  
      <div class="alert alert-success">
           <h3>Sorteerlijst FKS</h3>
               <table class="table">
                   <thead>
                       <tr>
                           <th>
                               Postcode
                           </th>
                           <th>
                               Route
                           </th>
                           <th>
                               Kies
                           </th>
                       </tr>
                   </thead>
               <tbody>';
        
            foreach($matches2 as $match2){
                echo '
                                   <tr class="clickable-row">
                                       <td>
                                           '.$match2["postcode"].'
                                       </td>
                                       <td>
                                           '.$match2["route"].'
                                       <td>    
                                           <div class="checkbox col-xs-3 col-sm-offset-2">
                                               <input type="checkbox" name="match2" value="'.$match2["id"].'">
                                           </div>
                                       </td>         
                               </tr>';
            }
        
        echo '
                        </tbody>
                    </table>
                </div>';
            }