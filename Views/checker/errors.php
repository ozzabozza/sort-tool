<?php
    if(count($errors) == 0){
            echo '  
                    <div class="alert alert-danger">
                        <h3>Errors</h3>
                        <b>Geen errors gevonden</b>
                    </div>';
    }
    else{
        echo '
<div class="col-xs-12">
   <h2>Errors</h2>
        <table class="table">
            <thead>
                <tr>
                    <th>
                        Postcode
                    </th>
                    <th>
                        Opmerking
                    </th>
                </tr>
            </thead>
           <tbody>';

        foreach($errors as $error){
            echo '
                 <tr class="danger">
                     <td>
                         '.$error["postcode"].'
                     </td>
                     <td>
                          '.$error["comment"].'
                     </td>
                 </tr>';
        }
    }?>
            </tbody>
        </table>    
    </div>