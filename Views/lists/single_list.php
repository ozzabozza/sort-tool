<?php
echo'
<div class="col-xs-12">
   <h2>'.ucfirst($list->getName()).'</h2>
        <table class="table">
            <thead>
                <tr>
                    <th>
                        Nummer
                    </th>
                    <th>
                        Adres
                    </th>
                </tr>
            </thead>
           <tbody>';

    foreach ($list->getAddresses() as $address){
        if($address["checked"] == 1){
            echo '
            	<tr class="success">';
            }
        elseif($address["checked"] == 0){
            echo '
            	<tr class="danger">';
            }
            echo'
                	<td>
                        '.$address["number"].'
                    </td>
                    <td>
                        '.$address["address"].'
                    </td>
                </tr>';
    }
       
echo '
            </tbody>
        </table>
</div>';
?>
