<?php
foreach ($lists as $list) {
echo'
<div class="col-xs-12">
   <h2>'.ucfirst($list->getName()).'</h2>
        <table class="table">
            <thead>
                <tr>
                    <th>
                        Nummer
                    </th>
                    <th>
                        Adres
                    </th>
                </tr>
            </thead>
           <tbody>';
    foreach ($list->getAddresses() as $address){
        if($address["checked"] == 1){
            echo '
            	<tr class="success">';
        }
        elseif($address["checked"] == 0){
            echo '
            	<tr class="danger">';
        }
        echo'
                	<td>
                        '.$address["number"].'
                    </td>
                    <td>
                        '.$address["address"].'
                    </td>
                </tr>';
    }

echo'         </tbody>
        </table>
</div>';
}
?>

<div class="col-xs-12">
       <?php echo '<h2>'.ucfirst($total->getName()).'</h2>'; ?>
        <table class="table">
            <thead>
                <tr>
                    <th>
                    Postcode
                    </th>
                    <th>
                    Route
                    </th>
                </tr>
            </thead>
            <tbody>
<?php
    foreach ($total->getAddresses() as $row) {
        if($row["checked"] == 1){
            echo '
                <tr class="success">';
        }
        elseif($row["checked"] == 0){
            echo '
                <tr class="danger">';
        }

        echo '
                    <td>
                    '.$row["postcode"].'
                    </td>
                    <td>
                    '.$row["route"].'
                    </td>
                </tr>';
    }
?>
            </tbody>
        </table>
</div>
