<?php

class Db{

	private static $instance =NULL; //Value will always present current state, no matter if object is create again.

	private function __construct(){}
     
	private function __clone(){}

	private static $databaseName = 'sorting_tool';
	private static $databaseUser = 'root';
	private static $databasePassword = 'root';

	public static function getInstance() {            
		if (!isset(self::$instance)){
			self::$instance = $pdo = new PDO('mysql:host=localhost;dbname='.self::$databaseName, self::$databaseUser, self::$databasePassword);
		}

		return self::$instance;
	}
}

?>